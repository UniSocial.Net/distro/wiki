`wiki & documentation!`

# _Foundry_
Foundry is a core Distirbution of UniSocial & UniFS. "core", meaning it's maintained in house & by our team friends & allies.

# App
It's also an App! Has apps for every OS!

# /Home/me
It uses the default UniFS services and Folder layout including:
- /Omni/Uni/{{ my }}
- LifeGraph, Lighthouse, K, Photon, code
- Query
- Vault
- 'Magic folders'

Gives you sofisticated workflows for editing & publishing content!

# Enancement modules
Has modules you can enable! Each is independent and you change it's startup!

major modules:
- AppStore
- Ark.Net
- Avana Music
- BlackCube.AI
- DreamAds
- Photon
- Pegasus Browser


# Runtime mode:
Has 2 runtimes modes:

- mini: is minimalist for "Personal Area Networks"
- Full: is a fullstack Unicloud engine capable of generating Wealth

Chose based on the power of your Hw and whether you want to generate Wealth / unicoin. You can change it anytime later!

Both include AppStore.

## AppStore
Let's you run 3rd party Apps. Some examples:
- Bittorrent Sync
- Kodi, Venom
- IPFS
- FreeNAS, openmediavault
- OpenCart
- Mailspring, Evolution
- Perkeep
- Torrent
- Wordpress


## mini
for "Personal Area Network"
*Only includes core FS & Cloud features.* _And is capable of mining but storage & Ark.Net only!_ which is usually "break-even": enough to pay for your hardware but doesn't generate much Wealth.

Has:
- NextCloud as main Web feature!
- Perkeep
- Wordpress


## Full
is fullstack Unicloud! lets you run large clusters that generate revenue! Capable of hosting desktops & Instances, serving optimized requests, peering, and transcoding data.

Has:
- full Unisocial.net Diaspora instance!

Cloud modules:
- Metal.Provision
- Ansible.Tower
- Hyperion
- EdgeFS
- Ark.Net: CDN service
- PaaS: OpenShift, CloudFoundry, Kubernetes
- Photon.Net: media & VR hosting
- UniNetwork.SDN: Security, Firewall, VPN,


## License:
Foundry is Apache licensed, but every component & module is GPL. Meaning you can customize and offer & Sell your own version of Foundry to the market with proprietary code! - provided that if you improve it's modules, you contribute back.
