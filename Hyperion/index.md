# Nix:
## Container
### Docker
- https://www.vagrantup.com/docs/provisioning/docker.html

### LXD
- https://www.catalyst.net.nz/blog/vagrant-and-lxd

## Hypervisor
### KVM
- https://github.com/vagrant-libvirt/vagrant-libvirt

### Xen
- https://github.com/xapi-project/vagrant-xenserver
- https://github.com/Wenzel/vagrant-xen

# Windows:
- https://www.vagrantup.com/docs/hyperv/

# Mac:
## Hypervisor
- https://www.google.com/search?q=vagrant+bhyve
- https://www.google.com/search?q=vagrant+hyperkit

[Forks](https://github.com/oldpatricka/vagrant-xhyve/network)
- https://github.com/sglover/vagrant-hyperkit

## Container
- https://github.com/ebarriosjr/potMachine